/**
 * Created by ahmad.kabakibi on 8/2/2015.
 */
/**
 * Created by ahmad.kabakibi on 7/18/2015.
 */

var express = require('express');
var http = require('http');
var streamify = require('streamify'),
    path = require('path'),
    url = require('url');
var fs = require('fs');
var request = require('request');
var _db = require('lib/db');
var alreadyReadDB = false;
var Logger = require('lib/Logs/logger');
require('linqjs') //microsoft linq style query module

var sutils = require('lib/Config/yliveutils.js'), config;

sutils.readConfig(function () {
    config = sutils.getconfig();
});

var SocketStream = require('socket.io-stream'); // Socket Stream to send Stream to Cache Server
var ioClient = require('socket.io-client'); // Client to fetch stream from Cache Server
var io = require('socket.io').listen(config.Settings.CachingServerPort);
var socket = ioClient.connect(config.Settings.CachingServer + ':' + config.Settings.CachingServerPort);
Logger.System("Cache Server :" + config.Settings.CachingServer + ':' + config.Settings.CachingServerPort);

//var socketFetchingCache = ioClient.connect(config.Settings.FetchingCache + ':' + config.Settings.FetchingCachePort);
Logger.System("FetchCache Server: " + config.Settings.FetchingCache + ':' + config.Settings.FetchingCachePort);
//var streamFetchingCache = SocketStream.createStream();

var self = this;
var events = require('events');
events.EventEmitter.call(self);
self.__proto__ = events.EventEmitter.prototype;

var moduleEvents = {
    onCachingProgress: 'onCachingProgress',
    onError: 'onError',
    onCacheStreaming: 'onCacheStreaming',
    onCacheRank: 'onCacheRank'
};

var app = express();

app.set('port', process.env.PORT || 2000);
app.set('views', __dirname + '/views');

app.get('/', function (req, res) {
    console.log('Route get db Status: ' + alreadyReadDB);
    //res.render('index.html');
    res.write(
        '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">' +
        '<head>' +
        '<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">' +
        ' <script src="//vjs.zencdn.net/4.12/video.js"></script>' +
        '</head>' +
        '<title></title></head>' +
        '<body> <table style="padding: 8px"><tbody><tr>'
    );
    _db.MostView(function (list) {
        list.forEach(function (value) {
            console.log("Rank " + value.videoID + " : " + value.videoViews);
            //HTML DOM Render Most View Top 3
            res.write(
                '<td> <a target="_blank"  href=http://' + req.get('host') + '/playback?id=' + value.videoID + '><img height="180" width="280" src=' + value.videoThumble + '><p>' + value.videoTitle + '</p></a> </td> '
            );
        });
        res.write('</tr></tbody></table> </body> </html>');
        res.end();
    });
});

var socketFetchingCache = ioClient.connect(config.Settings.FetchingCache + ':' + config.Settings.FetchingCachePort);


app.get('/playback', function (req, res) {
    var dataUrl = url.parse(req.url, true).query;
    var idQuality = dataUrl.quality || 'best';
    var quality = "-f," + idQuality;
    var streamQuality = idQuality == 'best' ? "" : '&quality=' + idQuality;

    socketFetchingCache.emit('getInfo',dataUrl.id,quality);

    socketFetchingCache.on('youtube-info',function(info){
        res.write(
            '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">' +
            '<head>' +
            '<link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">' +
            ' <script src="//vjs.zencdn.net/4.12/video.js"></script>' +
            '</head>' +
            '<title></title></head>' +
            '<body> <table style="padding: 8px"><tbody><tr>'
        );

        res.write(
            '<td><video id="video_1" class="video-js vjs-default-skin vjs-big-play-centered" autoplay controls preload="auto" width="640" height="400" data-setup="{}">' +
            ' <source src="http://' + req.get('host') + '/stream?id=' + dataUrl.id + streamQuality + '&ext=' + info.ext + '" type="video/mp4">' +
            ' <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video</p>' +
            ' </video></br><p>ex: http://HOSTURL/playback?id=/id/&quality=/Quality_id/ </p></td></tr> </tbody></table>'
        );

        info.formats.filter(function (item) {
           res.write('<h5>Quality: ' + item.format + ' ,Quality_id:' + item.format_id + '</h5>');
         });

        res.write(' </body> </html>');
      //  res.end();
    });
});

app.get('/stream', function (req, res) {
    var dataUrl = url.parse(req.url, true).query;
    var idQuality = dataUrl.quality || 'best';
    var quality = "-f," + idQuality;
    var filePath;
    var videoCachedID = dataUrl.id + '_' + idQuality;
    _db.refreshRank(videoCachedID, function (list) {
    //adding fs.existsSync to server.js to prevent file and db sync issue that can cause server.js to crash
        if (list.length > 0 && fs.existsSync(filePath)) {
            //self.emit(moduleEvents.onCacheStreaming, list);
            var rank = list[0].videoViews + 1;
            _db.incViewRank(dataUrl.id, rank);
            filePath = path.join(__dirname, 'videos', dataUrl.id + '_' + idQuality + '.' + dataUrl.ext);
            //stream video id from Storage Server
            var range = typeof req.headers.range === "string" ? req.headers.range : undefined;
            var start, end, chunksize;
            fs.stat(filePath, function (err, stats) {
                var total = stats.size;
                if (range !== undefined && (range = range.match(/bytes=(.+)-(.+)?/)) !== null) {
                    var parts = req.headers['range'].replace(/bytes=/, "").split("-");
                    start = parseInt(parts[0], 10);
                    end = parts[1] ? parseInt(parts[1], 10) : total - 1;
                    chunksize = (end - start) + 1;
                } else {
                    start = 0;
                    end = total - 1;
                    chunksize = (end - start) + 1;
                }

                res.writeHead(206, {
                    "Content-Range": "bytes " + start + "-" + end + "/" + total,
                    "Accept-Ranges": "bytes",
                    "Content-Length": chunksize,
                    "Content-Type": "video/" + dataUrl.ext
                });

                var stream = fs.createReadStream(filePath, {start: start, end: end})
                    .on("open", function () {
                        Logger.System('Streaming from Caching ' + filePath);
                        stream.pipe(res);
                    }).on("error", function (err) {
                        res.end(err);
                    });
            });
        }
        else {
            var output,outputTemp;
            output = path.join(__dirname, 'videos', dataUrl.id + '_' + idQuality + '.' + dataUrl.ext);
            outputTemp = path.join(__dirname, 'videos', dataUrl.id + '_' + idQuality+ Math.floor(Math.random() * (1000000 - 10 + 1) + 10) + '.' + dataUrl.ext);

            var reqURL = config.Settings.FetchingCache + ':' + config.Settings.FetchingCachePort + '/fetching?id=' + dataUrl.id + '&quality=' + quality;

            var stream = streamify({
                superCtor: http.ClientResponse,
                readable: true,
                writable: false
            });

            http.get(reqURL, function onResponse(response) {
                stream.resolve(response);
            });
            //Logger.System('Caching ' + dataUrl.id + '_' + idQuality + '.' + dataUrl.ext);

            var streamFetch = SocketStream.createStream();
            SocketStream(socket).emit('onCachingProgressSocket', streamFetch, output, outputTemp);
            stream.pipe(streamFetch);

            //stream.pipe(fs.createWriteStream(outputTemp));
            stream.pipe(res);

          /* socketFetchingCache.on('youtube-FetchingEnd',function(data,videoRequestJSON){
               //dataUrl.id is new request we are going to cache it in db
               if (fs.existsSync(output)) {
                   Logger.System('Already Cached: ' + data+'.' + dataUrl.ext);
                   fs.unlink(outputTemp,function(){
                       Logger.System('Already Cached: ' + data+'.' + dataUrl.ext+' Deleteing Temp Path Request: '+outputTemp);
                   });
                     //res.end();
               } else {
                   fs.renameSync(outputTemp, output);
                   Logger.System('Cached: ' + data+'.' + dataUrl.ext,' Name: '+output);
                   _db.insertVideo(videoRequestJSON, function (err, msg) {
                       Logger.System(msg);
                   });
                  // res.end();
               }
            });*/
        }
    });
});

io.on('connection', function (socket) {
    SocketStream(socket).on('onCachingProgressSocket', function (stream, output, outputTemp) {

        Logger.VideoRequest('Caching ' + output);

        stream.pipe(fs.createWriteStream(outputTemp));

        socketFetchingCache.on('youtube-FetchingEnd',function(data,videoRequestJSON){
            //dataUrl.id is new request we are going to cache it in db
            if (fs.existsSync(output)) {
                Logger.System('Already Cached: ' + data);
                fs.unlink(outputTemp,function(){
                    Logger.System('Already Cached: ' + data+' Deleteing Temp Path Request: '+outputTemp);
                });
            } else {
                fs.renameSync(outputTemp, output);
                Logger.System('Cached: ' + data+ ' Name: '+output);
                _db.insertVideo(videoRequestJSON, function (err, msg) {
                    Logger.System(msg);
                });
            }
        });
    });
});

app.get('/about', function (req, res) {
    res.sendFile(__dirname + '/views/about.html');
});
//adding crossdomain.xml
app.get('/crossdomain.xml',function(req,res){
    res.end('<?xml version="1.0"?>\n'+
            '<!DOCTYPE cross-domain-policy SYSTEM\n'+
            '"http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n'+
            '<cross-domain-policy>\n'+
            '<allow-access-from domain="*.cooliris.com" secure="false" />\n'+
            '</cross-domain-policy>\n'
            );
    });

// Listen on the specified IP and port.
http.createServer(app).listen(app.get('port'), function () {
    Logger.System("Server listening on port " + app.get('port'));
    _db.init(function (status) {
        alreadyReadDB = status;
        Logger.System('DB Status: ' + alreadyReadDB);
    });
});

