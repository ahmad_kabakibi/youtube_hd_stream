/**
 * Created by ahmad.kabakibi on 8/4/2015.
 */

// Server

var app = require('express')();
var url = require('url');
var Logger = require('lib/Logs/logger'),
    ytdl = require('lib/youtube-dl');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.set('port', process.env.PORT || 9124);
app.set('views', __dirname + '/views');
//app.use(app.router);

http.listen(app.get('port'), function () {
    Logger.System("Server listening on port " + app.get('port'));
});
var videoRequestJSON;

app.get('/fetching', function (req, res) {
    var dataUrl = url.parse(req.url, true).query;
    var video = ytdl('http://www.youtube.com/watch?v=' + dataUrl.id, [dataUrl.quality]);
    var quality=dataUrl.quality.split(",");
    var sizeload;
    video.on('info', function (info) {
        sizeload = info.size;
        res.writeHead(200, {
            "Content-Type": "video/" + info.ext
        });

        videoRequestJSON = {
            videoID: info.id,
            videoCachID: dataUrl.id + '_' + quality[1],
            videoLink: info.url,
            videoTitle: info.title,
            videoThumble: info.thumbnail,
            videoViews: 1
        };
        Logger.VideoFetchingRequest('OnYoutube-fetching ' + dataUrl.id+'_'+quality[1]);
        video.pipe(res);
    });
    var pos = 0;
    video.on('data', function (data) {
        pos += data.length;
        // `size` should not be 0 here.
        if (sizeload) {
            var percent = (pos / sizeload * 100).toFixed(2);
            Logger.VideoFetchingRequest('Caching percent:' + '% ' + percent + ' [' + dataUrl.id + '_' + dataUrl.quality + ']');
        }
    });

    video.on('end', function () {
        var quality=dataUrl.quality.split(",");
        Logger.VideoFetchingRequest('On Youtube-fetching End ' + dataUrl.id+'_'+quality[1]);
        io.emit('youtube-FetchingEnd',dataUrl.id + '_' + quality[1],videoRequestJSON);
    });

});


io.on('connection', function(socket){
    socket.on('getInfo', function (dataUrlID, quality) {
        ytdl.getInfo('http://www.youtube.com/watch?v=' + dataUrlID, [quality], function (err, info) {
            if (err) {
                Logger.VideoFetchingRequest('youtube Request Error' + err);
                // res.end(500);
            }
            Logger.VideoFetchingRequest('On Youtube-getInfo ' + dataUrlID + '.' + info.ext);
            io.emit('youtube-info', info);
        });
    });
});

