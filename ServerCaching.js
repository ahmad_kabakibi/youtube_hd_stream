

var SocketStream = require('socket.io-stream');
var fs = require('fs'), path = require('path');
var Logger = require('lib/Logs/logger');
// Server
var io = require('socket.io').listen(9123);
//var streamServer;
var output, outputTemp, filePath;


io.on('connection', function (socket) {
    SocketStream(socket).on('onCachingProgressSocket', function (stream, fileName, fileNameTmp) {
        output = path.join(__dirname, 'videos', fileName);
        outputTemp = path.join(__dirname, 'videos', fileNameTmp);

        console.log('Caching Progress working ... :' + output);
        stream.pipe(fs.createWriteStream(outputTemp));

        stream.on('end', function () {

            if (fs.existsSync(output)) {
                Logger.System('Already Cached: ' + dataUrl.id + '_' + idQuality);
            } else {
                fs.renameSync(outputTemp, output);
                socket.emit('StreamEndCachingSocket', output);
            }
        });
    });


    socket.on('CacheStreamingSocketfilePathSize', function (filePathName) {
        var streamServer = SocketStream.createStream();

        filePath = path.join(__dirname, 'videos', filePathName);
        // Logger.System('Streaming from Cache ' + filePath);
        fs.stat(filePath, function (err, stats) {
            var total = stats.size;
            socket.emit('onCacheStreamFileSize', filePathName, total);

            socket.on('CacheStreamingSocket', function (filePathName, start, end) {

                SocketStream(socket).emit('onCacheStreamingSocket', streamServer, filePathName);

                var ReadStream = fs.createReadStream(filePath, {start: start, end: end}).on("open", function () {
                    Logger.System('Streaming from Cache ' + filePath);
                    ReadStream.pipe(streamServer);
                });
            });
        });
    });
});