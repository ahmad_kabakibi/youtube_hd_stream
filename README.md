YLiveStream
====================

Streaming YouTube's video

Setup
====================
Just execute this on your console:
	$ cd ./YLiveStream
	$ npm install .
	$ node Server.js
	
_(Remember you'll need to have node.js installed to use this!)_

How to use
====================

Easy! browser and load this URL:

	http://localhost:2000/
	http://localhost:2000/playback?id=<YouTube video ID here>
	http://localhost:2000/playback?id=<YouTube video ID here>&quality=<video quality here>
	
Then you should see the video loaded.

by Ahmad Kabaibi
ahmadkbakibi@gmail.com